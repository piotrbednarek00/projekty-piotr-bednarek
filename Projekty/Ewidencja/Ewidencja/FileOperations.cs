﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Ewidencja
{
    public class FileOperations
    {

        public static string AddFile()
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".pdf";
            dlg.Filter = "PDF (*.pdf)|*.pdf|DOCX (*.docx)|*.docx|DOC (*.doc)|*.doc|ANY (*.*)|*.*";

            dlg.Multiselect = false;
            dlg.Title = "Wybierz pliki";
            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                return dlg.FileName+";";
            }
            return "";
        }

        public static List<FileButton> ZnajdzPliki(ObiektEwidencji obiekt)
        {
            if (obiekt == null || obiekt.Files == null || obiekt.Files == "") return null;
            List<FileButton> przyciski = new List<FileButton>();
            string pliki = obiekt.Files;
            int size = pliki.Length, poczatek = 0;
            for (int i = 0; i < size; i++)
            {
                if (pliki[i] == ';')
                {
                    string temp = pliki.Substring(poczatek, i - poczatek);
                    for (int j = temp.Length - 1; j > 0; j--)
                    {
                        if (temp[j] == '\\')
                        {
                            string file_name = temp.Substring(j + 1, temp.Length - j - 1);
                            przyciski.Add(new FileButton { ButtonContent = file_name, FilePath = temp });
                            break;
                        }
                    }
                    poczatek = i + 1;
                }
            }
            return przyciski;
        }

        public static void OpenFile(string name)
        {
            try
            {
                System.Diagnostics.Process.Start(@"" + name);
            }
            catch (SystemException)
            {
                MessageBox.Show("Błędny plik", "Błąd", MessageBoxButton.OK);
            }
        }

    }
}
