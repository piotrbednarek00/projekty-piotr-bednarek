﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;

namespace Ewidencja
{
    /// <summary>
    /// Logika interakcji dla klasy AddItemWindows.xaml
    /// </summary>
    public partial class AddItemWindows : Window
    {
        ObiektEwidencji nowy = new ObiektEwidencji();
        public AddItemWindows()
        {
            InitializeComponent();
            OnStart();
        }

        private void OnStart()
        {
            AddData.SelectedDate = DateTime.Now.Date;
        }
        private bool IfStringIsEmpty(string[] line, int size)
        {
            for (int i = 0; i < size; i++)
            {
                if (line[i] == "")
                {
                    return true;
                }
            }
            return false;
        }

        protected virtual bool IsFileinUse(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return false;
        }

        private void AddItem(object sender, RoutedEventArgs e)
        {
            DateTime czas = default(DateTime);
            if(Data.SelectedDate!=null) czas = Data.SelectedDate.Value;
            string[] lines = { MainWindow.itemsNumber+1.ToString(), Nazwa.Text, Miejsce.Text, Opis.Text, AddData.Text};
            
            int size = lines.Length;

            if (IfStringIsEmpty(lines, size))
            {
                MessageBox.Show("Proszę wypełnić potrzebne pola");
            }
            else
            {
                
                nowy.Description= Opis.Text; 
                nowy.Name= Nazwa.Text;
                nowy.Place= Miejsce.Text;
                nowy.Date = Data.SelectedDate != null ? czas.ToString("dd.MM.yyyy") : "";
                nowy.AddDate = AddData.SelectedDate != null ? AddData.SelectedDate.Value.ToString("dd.MM.yyyy") : "";
                SqliteDataAccess.SavePerson(nowy);
                ((MainWindow)System.Windows.Application.Current.MainWindow).DownloadData();
                this.Close();
            }
          
        }
        private void DodajPlik(object sender, RoutedEventArgs e)
        {
            string temp = FileOperations.AddFile(), file_name="";
            temp= temp.Substring(0, temp.Length-1);
            for (int j = temp.Length - 1; j > 0; j--)
            {
                if (temp[j] == '\\')
                {
                    file_name = temp.Substring(j + 1, temp.Length - j - 1);
                    break;
                }
            }
            if(!Directory.Exists(MainWindow.appPath + "\\" + MainWindow.filesDirectories))
            {
                Directory.CreateDirectory(MainWindow.appPath + "\\" + MainWindow.filesDirectories);
            }
            File.Copy(temp, MainWindow.appPath+"\\"+MainWindow.filesDirectories+"\\"+ file_name, true);
            nowy.Files = string.Concat(nowy.Files, MainWindow.appPath + "\\" + MainWindow.filesDirectories + "\\" + file_name+";");
            OdswierzListePlikow();

        }

        void OdswierzListePlikow()
        {
            List<FileButton> przyciski = FileOperations.ZnajdzPliki(nowy);
            ic.ItemsSource = przyciski;
        }

        private void DeleteFile(object sender, RoutedEventArgs e)
        {
            if (ic.Items.Count < 1) return;
            FileButton obiekt = (FileButton)ic.SelectedItems[0];
            File.Delete(obiekt.FilePath);
            nowy.Files = nowy.Files.Replace(obiekt.FilePath + ";", "");
            OdswierzListePlikow();
        }

        private void OtworzPlik(object sender, RoutedEventArgs e)
        {
            if (ic.Items.Count < 1) return;
            FileButton obiekt = (FileButton)ic.SelectedItems[0];
            obiekt.OtworzPlik();
        }

    }
}
