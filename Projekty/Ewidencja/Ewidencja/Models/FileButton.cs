﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ewidencja
{
    public class FileButton
    {
        public string ButtonContent { get; set; }
        public string FilePath { get; set; }

        public void OtworzPlik()
        {
            FileOperations.OpenFile(FilePath);
        }
    }
}
