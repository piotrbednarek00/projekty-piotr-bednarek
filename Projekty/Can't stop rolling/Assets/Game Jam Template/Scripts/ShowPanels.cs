﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShowPanels : MonoBehaviour {

	public GameObject optionsPanel;							//Store a reference to the Game Object OptionsPanel 
	public GameObject optionsTint;							//Store a reference to the Game Object OptionsTint 
	public GameObject menuPanel;							//Store a reference to the Game Object MenuPanel 
	//public GameObject pausePanel;	                        //Store a reference to the Game Object PausePanel 
    public GameObject SklepPanel;
    public GameObject highscore;
    public GameObject highscoreTint;
    public GameObject missions;
    public GameObject missionsTint;
    public GameObject maszyna;
    public GameObject masyznaTint;
    public GameObject powiadomienie;
    public GameObject powiadomienieTint;

    public Text textM1;
    public Text textM2;
    public Text textM3;
    public Text zrobioneMisje;

    private void Awake()
    {
        PlayerPrefs.GetInt("zrobioneMisje", 0);
    }

    private void Update()
    {
        zrobioneMisje.text = PlayerPrefs.GetInt("zrobioneMisje", 0).ToString();
        //misja1
        if (PlayerPrefs.GetInt("Challenge1.1", 0) != 1)
        {
            textM1.text = "Get 750 highscore";
        }
        else if (PlayerPrefs.GetInt("Challenge2.1", 0) != 1 && PlayerPrefs.GetInt("Challenge1.1") == 1)
        {
            textM1.text = "Get 1000 highscore";
        }
        else if (PlayerPrefs.GetInt("Challenge3.1", 0) != 1 && PlayerPrefs.GetInt("Challenge2.1", 0) == 1 && PlayerPrefs.GetInt("Challenge1.1") == 1)
        {
            textM1.text = "Get 1250 highscore";
        }
        else if (PlayerPrefs.GetInt("Challenge4.1", 0) != 1 && PlayerPrefs.GetInt("Challenge3.1", 0) == 1 && PlayerPrefs.GetInt("Challenge2.1", 0) == 1 && PlayerPrefs.GetInt("Challenge1.1") == 1)
        {
            textM1.text = "Get 1500 highscore";
        }
        else textM1.text = "All challenges done here";
        //misja2
        if (PlayerPrefs.GetInt("Challenge1.2", 0) != 1)
        {
            textM2.text = "Acquire 2 labels";
        }
        else if (PlayerPrefs.GetInt("Challenge2.2", 0) != 1 && PlayerPrefs.GetInt("Challenge1.2") == 1)
        {
            textM2.text = "Acquire 5 labels";
        }
        else if (PlayerPrefs.GetInt("Challenge3.2", 0) != 1 && PlayerPrefs.GetInt("Challenge2.2", 0) == 1 && PlayerPrefs.GetInt("Challenge1.2") == 1)
        {
            textM2.text = "Acquire 8 labels";
        }
        else if (PlayerPrefs.GetInt("Challenge4.2", 0) != 1 && PlayerPrefs.GetInt("Challenge3.2", 0) == 1 && PlayerPrefs.GetInt("Challenge2.2", 0) == 1 && PlayerPrefs.GetInt("Challenge1.2") == 1)
        {
            textM2.text = "Acquire 10 labels";
        }
        else textM2.text = "All challenges done here";
        //misja3
        if (PlayerPrefs.GetInt("Challenge1.3", 0) != 1)
        {
            textM3.text = "Collect 10 blue caps";
        }
        else if (PlayerPrefs.GetInt("Challenge2.3", 0) != 1 && PlayerPrefs.GetInt("Challenge1.3") == 1)
        {
            textM3.text = "Jump over the hydrant 10 times";
        }
        else if (PlayerPrefs.GetInt("Challenge3.3", 0) != 1 && PlayerPrefs.GetInt("Challenge2.3", 0) == 1 && PlayerPrefs.GetInt("Challenge1.3") == 1)
        {
            textM3.text = "Collect 10 red caps";
        }
        else if (PlayerPrefs.GetInt("Challenge4.3", 0) != 1 && PlayerPrefs.GetInt("Challenge3.3", 0) == 1 && PlayerPrefs.GetInt("Challenge2.3", 0) == 1 && PlayerPrefs.GetInt("Challenge1.3") == 1)
        {
            textM3.text = "Jump over the hole 10 times";
        }
        else textM3.text = "All challenges done here";
    }


    //Call this function to activate and display the Options panel during the main menu
    public void ShowOptionsPanel()
	{
		optionsPanel.SetActive(true);
		optionsTint.SetActive(true);
	}

	//Call this function to deactivate and hide the Options panel during the main menu
	public void HideOptionsPanel()
	{
		optionsPanel.SetActive(false);
		optionsTint.SetActive(false);
        Debug.Log("HIDE");
	}

	//Call this function to activate and display the main menu panel during the main menu
	public void ShowMenu()
	{
		menuPanel.SetActive (true);
	}

	//Call this function to deactivate and hide the main menu panel during the main menu
	public void HideMenu()
	{
		menuPanel.SetActive (false);
	}

	public void ShowHighscore()
	{
		highscore.SetActive (true);
        highscoreTint.SetActive(true);
    }

	public void HideHighscore()
	{
		highscore.SetActive (false);
        highscoreTint.SetActive(false);
    }

	//Call this function to activate and display the Pause panel during game play
	//public void ShowPausePanel()
	//{
	//	pausePanel.SetActive (true);
	//	optionsTint.SetActive(true);
	//}

	//Call this function to deactivate and hide the Pause panel during game play
	//public void HidePausePanel()
	//{
	//	pausePanel.SetActive (false);
	//	optionsTint.SetActive(false);

	//}
    public void ShowSklepPanel()
    {
       // pausePanel.SetActive(false);
        optionsTint.SetActive(false);
    }

	public void ShowMissions()
	{
		missions.SetActive(true);
        missionsTint.SetActive(true);
	}

	public void HideMissions()
	{
		missions.SetActive(false);
		missionsTint.SetActive(false);
	}

	public void ShowMaszyna()
	{
		maszyna.SetActive (true);
    }

	public void HideMaszyna()
	{
		maszyna.SetActive (false);
	}

	public void ShowPowiadomienie()
	{
		powiadomienie.SetActive (true);
        powiadomienieTint.SetActive(true);
    }

	public void HidePowiadomienie()
	{
		powiadomienie.SetActive (false);
        powiadomienieTint.SetActive(false);
    }
}
