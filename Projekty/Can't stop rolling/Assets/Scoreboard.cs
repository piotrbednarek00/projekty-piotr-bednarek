﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scoreboard : MonoBehaviour
{
    [SerializeField] private Sprite[] obrazkiButelek;
    [SerializeField] private Image bottleImage;
    private void Start()
    {
        int indexStart = PlayerPrefs.GetInt("skin");
        bottleImage.sprite = obrazkiButelek[indexStart];
    }
}
