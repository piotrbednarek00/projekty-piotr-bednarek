﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EkranLadowania : MonoBehaviour {
	
	public static EkranLadowania Instance{ set; get;}

	public void Awake()
	{
		Instance = this;
		Load("Palyer");
		Load("Tile1");
		Load("Tile2");
	}

	public void Load(string sceneName)
	{
		if (!SceneManager.GetSceneByName (sceneName).isLoaded)
			SceneManager.LoadScene (sceneName, LoadSceneMode.Additive);
	}

	public void Unload(string sceneName)
	{
		if (SceneManager.GetSceneByName (sceneName).isLoaded)
			SceneManager.UnloadScene (sceneName);
	}

}