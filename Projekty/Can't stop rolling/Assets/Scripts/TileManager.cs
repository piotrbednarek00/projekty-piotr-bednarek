﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileManager : MonoBehaviour {

	public GameObject[] tilePrefabs;
    public GameObject carPrefab;
	public GameObject bokPrefab;
    int min = 0;
    int max = 100;
    public int carSpawn = 50;
	public int bokSpawn = 50;
    int franek;
    float samochod;
	float bok;
    int a = 0;
	public GameObject car;
    //GameObject car2;


    private Transform playerTransform;
	private float spawnZ = 90.0f;
	private float tileLength = 45.0f;
	private float safeZone = -50.0f;
	private int amountTilesOnScreen = 2;
	private int lastPrefabIndex = 0;
	
	private List<GameObject> activeTiles;
    List<GameObject> samochody;
    //List<GameObject> samochody2;
    List<GameObject> boki;

	// Use this for initialization
	private void Start () {
        a = 0;
		activeTiles = new List<GameObject>();
        samochody = new List<GameObject>();
        //samochody2 = new List<GameObject>();
        boki = new List<GameObject>();
		playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
		
		for (int i = 0; i < amountTilesOnScreen; i++) 
		{
			if( i < 18)
				SpawnTile();
			else 
			SpawnTile();
			SpawnTile();
		}
	}
	
	// Update is called once per frame
	private void FixedUpdate ()
    {
        if (playerTransform.position.z - safeZone > (spawnZ - amountTilesOnScreen * tileLength))
        {
			SpawnTile();
			DeleteTile();
			//DeleteBok();
		}
		car.transform.position += new Vector3 (0, 0, -0.7f);
        //car2.transform.position += new Vector3(0, 0, 0.7f);

    }
    int b = 1;
	private void SpawnTile(int prefabIndex = -1)
    {
        franek = Random.Range(min, max);
        samochod = Random.Range(spawnZ - 7, spawnZ + 7);
        if ((franek > carSpawn) && (Time.timeSinceLevelLoad > 1))
        {
            if (Time.timeSinceLevelLoad > 10)
            {
                for (int i=0; i<100; i++)
                {
                    //car.transform.position += new Vector3(0, 0, -0.1f);
                }
            }
            car = Instantiate(carPrefab, new Vector3(-10f, 0f, samochod), Quaternion.Euler(0, -90, 0)) as GameObject;
            //car2 = Instantiate(carPrefab, new Vector3(-10f, 0f, samochod), Quaternion.Euler(0, -90, 0)) as GameObject;
            car.transform.SetParent(transform);
            //car2.transform.SetParent(transform);
            samochody.Add(car);
            //samochody2.Add(car);
            if (b>2)
            {
                DeleteCar();
            }
            b++; 
        }
		/*	
		bok = Random.Range(spawnZ - 0, spawnZ + 0);
		GameObject boczny;
		boczny = Instantiate (bokPrefab, new Vector3 (-28.2f, 0f, bok), Quaternion.Euler (0, 180, 0)) as GameObject;
		boczny.transform.SetParent(transform);
		boki.Add(boczny);
        */

		GameObject go; 
		if(prefabIndex == -1)
			go = Instantiate(tilePrefabs[RandomPrefabINdex()]) as GameObject;
		else 
			go = Instantiate(tilePrefabs[prefabIndex]) as GameObject;
		go.transform.SetParent(transform);
		go.transform.position = Vector3.forward * spawnZ;
		spawnZ += tileLength;
		activeTiles.Add(go);
        a++;
		
	}
    /*
	void DeleteBok()
	{
		Destroy(boki[0]);
		boki.RemoveAt(0);
	}
    */
    void DeleteCar()
    {
        Destroy(samochody[0]);
        //Destroy(samochody2[0]);
        samochody.RemoveAt(0);
        //samochody2.RemoveAt(0);
    }
	private void DeleteTile()
    {
		Destroy(activeTiles[0]);
		activeTiles.RemoveAt(0);
	}
	
	public int RandomPrefabINdex() {
		if(tilePrefabs.Length <= 1) 
			return 0;
		
		//int randomIndex = lastPrefabIndex;
		int randomIndex = Random.Range(0, tilePrefabs.Length);
		while(randomIndex == lastPrefabIndex) {
			randomIndex = Random.Range(0, tilePrefabs.Length);
		}
		
		lastPrefabIndex = randomIndex;
		return randomIndex;
	}

	public void Restart()
	{
		foreach(GameObject tile in activeTiles)
		{
			Destroy(tile);
		}
		activeTiles.Clear();
		samochody.Clear();
		boki.Clear();

		Start();
	}
}
