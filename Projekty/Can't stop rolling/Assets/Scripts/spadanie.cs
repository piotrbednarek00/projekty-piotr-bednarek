﻿using UnityEngine;
using System.Collections;

public class spadanie : MonoBehaviour {

	// Use this for initialization
	void Start () {
		//startOffset = transform.position;
	}

	public float speed;
	public Vector3 target = new Vector3(1.21f,0.17f,-7.83f);
	public AudioSource zrodloDzwieku;
	public AudioClip odglos;
	private bool detectedCollision = false;


	void Update() {
		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards(transform.position, target, step);
	}

	void OnCollisionEnter(Collision collision){
		if(!detectedCollision){
			//ContactPoint contactPoint = collision.contacts [0];

			zrodloDzwieku.PlayOneShot(odglos);
			detectedCollision = true;
			Debug.Log ("dupa1");
		} 
	}
	/*void OnCollisionStay(Collision collision){
		Debug.Log ("dupa1");
	}
	void OnCollisionExit(Collision collision){
		Debug.Log ("dupa2");
	}*/

}