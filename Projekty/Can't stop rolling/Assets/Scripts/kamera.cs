﻿using UnityEngine;
using System.Collections;

public class kamera : MonoBehaviour {

	public Transform lookAt;
	public Vector3 startOffset;
	public Vector3 moveVector;

	private float transition = 0.0f;
	private float animationDuration = 1f;
	private Vector3 animationOffset = new Vector3(0,4,4);

	// Use this for initialization
	void Start () {
		startOffset = transform.position - lookAt.position;
	}

	// Update is called once per frame
	void Update () {
		moveVector = lookAt.position + startOffset;

		moveVector.x = 0f;
		moveVector.y = Mathf.Clamp(moveVector.y,1,1);
		moveVector.z = -9.71f;

		if(transition > 1.0f)
		{	
			transform.position = moveVector;
		}
		else {
			transform.position = Vector3.Lerp(moveVector + animationOffset,moveVector,transition);
			transition += Time.deltaTime * 1 / animationDuration;
		}


	}
}