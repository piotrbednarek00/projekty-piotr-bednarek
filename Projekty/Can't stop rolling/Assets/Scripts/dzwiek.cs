﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dzwiek : MonoBehaviour {

	public int dzwiek1;
	public GameObject dzwiek2;
	public GameObject wyciszenie;
	// Use this for initialization
	void Start () {
		if (PlayerPrefs.HasKey ("dzwiek"))
			dzwiek1 = PlayerPrefs.GetInt ("dzwiek");
		else
			PlayerPrefs.SetInt("dzwiek", 1);
	}
	
	// Update is called once per frame
	void update () {
		if (dzwiek1 == 1) {
			AudioSource[] aSources = FindObjectsOfType (typeof(AudioSource)) as AudioSource[];
			foreach (AudioSource source in aSources) {
				source.Play ();
				dzwiek2.SetActive(true);
				wyciszenie.SetActive(false);
			}
		} 
		else {
			AudioSource[] aSources = FindObjectsOfType (typeof(AudioSource)) as AudioSource[];
			foreach (AudioSource source in aSources) {
				source.Pause ();
				dzwiek2.SetActive(false);
				wyciszenie.SetActive(true);
			}
		}
	}
	void akcja (){
		if (dzwiek1 == 1) {
			AudioSource[] aSources = FindObjectsOfType (typeof(AudioSource)) as AudioSource[];
			foreach (AudioSource source in aSources) {
				source.Play ();
				dzwiek2.SetActive(true);
				wyciszenie.SetActive(false);
			}
		} 
		else {
			AudioSource[] aSources = FindObjectsOfType (typeof(AudioSource)) as AudioSource[];
			foreach (AudioSource source in aSources) {
				source.Pause ();
				dzwiek2.SetActive(false);
				wyciszenie.SetActive(true);
			}
		}

	}
}
