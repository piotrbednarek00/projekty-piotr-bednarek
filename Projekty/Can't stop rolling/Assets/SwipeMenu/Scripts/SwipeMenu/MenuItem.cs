﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace SwipeMenu
{
	public class MenuItem : MonoBehaviour
	{
        ItemInfo itemInfo;
        [SerializeField]
        private TextMesh priceText;
        [SerializeField]
        private SpriteRenderer image;

        public int GetPrice()
        {
            return itemInfo.price;
        }

        public void SetValues(ItemInfo ItemInfo)
        {
            if (ItemInfo.isBought || ItemInfo.isSpecial) priceText.gameObject.SetActive(false);
            priceText.text = ItemInfo.price.ToString();
            name = ItemInfo.name;
            image.sprite = ItemInfo.image;
            itemInfo = ItemInfo;
        }
        
        public void OnCLickEvent()
        {
            ExampleCharacterSelector exampleCharacterSelector = FindObjectOfType<ExampleCharacterSelector>();
            exampleCharacterSelector.Select(itemInfo);
        }
        public Button.ButtonClickedEvent OnClick;
        
		public Button.ButtonClickedEvent OnOtherMenuClick;

	}
}