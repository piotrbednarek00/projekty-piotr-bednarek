﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class ExampleCharacterSelector : MonoBehaviour
{
	public Text text;
    [SerializeField]
    private Text moneyText;
    public int kasa,num;

	void Start(){
        if (PlayerPrefs.HasKey("pickup"))
            kasa = PlayerPrefs.GetInt("pickup");
        else
            PlayerPrefs.SetInt("pickup", 0);
        kasa = PlayerPrefs.GetInt("pickup");
        moneyText.text = kasa.ToString();

    }

	public void Select (ItemInfo itemInfo)
	{
        if(itemInfo.isBought)
        {
            PlayerPrefs.SetInt("skin", itemInfo.Id);
            LoadingScreenManager.LoadScene(num);
        }
        else
        {
            if (PlayerPrefs.HasKey("pickup"))
                kasa = PlayerPrefs.GetInt("pickup");
            else
                PlayerPrefs.SetInt("pickup", 0);
            kasa = PlayerPrefs.GetInt("pickup");
            if(kasa>= itemInfo.price)
            {
                Debug.Log("kupiles "+itemInfo.name);
                kasa -= itemInfo.price;
                PlayerPrefs.SetInt(itemInfo.name, 1);
                PlayerPrefs.SetInt("pickup", kasa);
                PlayerPrefs.SetInt("skin", itemInfo.Id);
                LoadingScreenManager.LoadScene(num);
            }
            else
            {
                Debug.Log("za malo kasy :(");
            }
        }
        
        
	}
}
