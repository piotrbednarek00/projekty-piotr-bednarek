﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetQuality : MonoBehaviour
{
    private void Start()
    {
        if (PlayerPrefs.HasKey("Quality"))
            SetQualityLvl(PlayerPrefs.GetInt("Quality"));
        else
        {
            PlayerPrefs.SetInt("Quality", 3);
            SetQualityLvl(3);
        }
    }
    public void SetQualityLvl(int level)
    {
        QualitySettings.SetQualityLevel(level, true);
    }
}
